package de.fewclicks.beacon.prototype.activites.map;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import de.fewclicks.beacon.prototype.R;
import de.fewclicks.beacon.prototype.controller.map.MapView;
import de.fewclicks.beacon.prototype.lateration.CartesianCoordinate;

public class MapActivity extends Activity
{
	private static final String TAG = "MapActivity";

	private MapView mapView;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		Log.i(TAG, "Create MapView");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		ViewGroup viewGroup = (ViewGroup) this
				.findViewById(R.id.map_layout_view);
		mapView = new MapView(this);
		mapView.setId(123);
		mapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		viewGroup.addView(mapView);
		Button button = (Button) this.findViewById(R.id.button1);
		TextView textViewX = (TextView) this.findViewById(R.id.editText1);
		TextView textViewY = (TextView) this.findViewById(R.id.editText2);
		button.bringToFront();
		textViewX.bringToFront();
		textViewY.bringToFront();
		setupActionBar();
	}

	public void setPosition(View view)
	{
		mapView = (MapView) this.findViewById(123);
		if (mapView == null)
		{
			mapView = new MapView(this);
		}
		TextView textViewX = (TextView) this.findViewById(R.id.editText1);
		TextView textViewY = (TextView) this.findViewById(R.id.editText2);
		CartesianCoordinate cartesianCoordinate = new CartesianCoordinate();
		cartesianCoordinate.setX(Double.parseDouble(textViewX.getText()
				.toString()));
		cartesianCoordinate.setY(Double.parseDouble(textViewY.getText()
				.toString()));
		mapView.setPosition(cartesianCoordinate);
	}

	private void setupActionBar()
	{

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
