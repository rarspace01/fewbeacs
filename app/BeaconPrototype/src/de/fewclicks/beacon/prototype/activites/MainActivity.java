package de.fewclicks.beacon.prototype.activites;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import de.fewclicks.beacon.prototype.R;
import de.fewclicks.beacon.prototype.activites.map.MapActivity;
import de.fewclicks.beacon.prototype.controller.beacon.ScanService;
import de.fewclicks.beacon.prototype.controller.beacon.ScanService.ScanServiceBinder;
import de.fewclicks.beacon.prototype.lateration.CartesianCoordinate;

public class MainActivity extends Activity {

	private static final String TAG = "MainActivity";

	private ScanService mService;

	// Constants
	// The authority for the sync adapter's content provider
	public static final String AUTHORITY = "de.fewclicks.beacon.prototype.beacon";
	// An account type, in the form of a domain name
	public static final String ACCOUNT_TYPE = "de.fewclicks.fewbeacs";
	// The account name
	public static final String ACCOUNT = "dummyaccount";
	// Instance fields
	private Account mAccount;

	// Sync interval constants
	public static final long MILLISECONDS_PER_SECOND = 1000L;
	public static final long SECONDS_PER_MINUTE = 60L;
	public static final long SYNC_INTERVAL_IN_MINUTES = 60L;
	public static final long SYNC_INTERVAL = SYNC_INTERVAL_IN_MINUTES
			* SECONDS_PER_MINUTE * MILLISECONDS_PER_SECOND;
	// Global variables
	// A content resolver for accessing the provider
	ContentResolver mResolver;

	private boolean mBound = false;

	private final ServiceConnection mConnection = new ServiceConnection() {
		private ScanServiceBinder binder;

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			binder = (ScanServiceBinder) service;
			mService = binder.getService();
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mBound = false;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		
		// Create the dummy account
		mAccount = CreateSyncAccount(this);

		// Get the content resolver for your app
        mResolver = getContentResolver();
        
        ContentResolver.setIsSyncable(mAccount, AUTHORITY, 1);
        
        ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);
        
        /*
         * Turn on periodic syncing
         */
        ContentResolver.addPeriodicSync(
                mAccount,
                AUTHORITY,
                new Bundle(),
                SYNC_INTERVAL);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void readService(View view) {
		Log.i(TAG, "reading from service");
		if (mService != null) {
			CartesianCoordinate cc = mService.getPosition();
			TextView tvLocation = (TextView) findViewById(R.id.txtLocation);
			tvLocation.setText("Location 2D: " + Math.round(cc.getX()) + "/"
					+ Math.round(cc.getY()));
		} else {
			Log.i(TAG, "Triggered but no Service");
		}
	}

	public void startService(View view) {

		Log.i(TAG, "binding service");

		Intent intent = new Intent(this, ScanService.class);
		// startService(intent);
		bindService(intent, // wie bei startService
				mConnection, // ServiceConnection //Flag, die das Verhältnis
								// von
								// Service und Acitivity festlegt
				Context.BIND_AUTO_CREATE);
	}

	public void startMap(View view)
	{
		Intent intent = new Intent(this, MapActivity.class);
		startActivity(intent);
	}
		/**
	 * Create a new dummy account for the sync adapter
	 * 
	 * @param context
	 *            The application context
	 * @return 
	 */
	public static Account CreateSyncAccount(Context context) {
		// Create the account type and default account
		Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
		// Get an instance of the Android account manager
		AccountManager accountManager = (AccountManager) context
				.getSystemService(ACCOUNT_SERVICE);
		/*
		 * Add the account and account type, no password or user data If
		 * successful, return the Account object, otherwise report an error.
		 */
		if (accountManager.addAccountExplicitly(newAccount, null, null)) {
			/*
			 * If you don't set android:syncable="true" in in your <provider>
			 * element in the manifest, then call context.setIsSyncable(account,
			 * AUTHORITY, 1) here.
			 */
		} else {
			/*
			 * The account exists or some other error occurred. Log this, report
			 * it, or handle it internally.
			 */
		}
		
		return newAccount;
	}

    /**
     * Respond to a button click by calling requestSync(). This is an
     * asynchronous operation.
     *
     * This method is attached to the refresh button in the layout
     * XML file
     *
     * @param v The View associated with the method call,
     * in this case a Button
     */
    public void onRefreshButtonClick(View v) {
    	Log.i(TAG,"clicked Fresh Button");
        // Pass the settings flags by inserting them in a bundle
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        /*
         * Request the sync for the default account, authority, and
         * manual sync settings
         */
        ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);
    }

}
