package de.fewclicks.beacon.prototype.lateration;

import java.util.List;

public interface ILateration {
	public CartesianCoordinate getLateration(List<GuessCoordinate> guessPoints);
	
}
