package de.fewclicks.beacon.prototype.lateration;

/**
 * code as CPOL license
 * http://www.codeproject.com/Articles/405128/Matrix-operations-in-Java
 */
public class NoSquareException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6584150372849833148L;

	public NoSquareException() {
		super();
	}

	public NoSquareException(String message) {
		super(message);
	}

}
