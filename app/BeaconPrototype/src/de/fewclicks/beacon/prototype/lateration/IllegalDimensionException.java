package de.fewclicks.beacon.prototype.lateration;

/**
 * code as CPOL license
 * http://www.codeproject.com/Articles/405128/Matrix-operations-in-Java
 */
public class IllegalDimensionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5381583154275330268L;

	public IllegalDimensionException() {
		super();
	}

	public IllegalDimensionException(String message) {
		super(message);
	}

}
