package de.fewclicks.beacon.prototype.lateration;

import java.util.List;

import android.util.Log;
import de.fewclicks.beacon.prototype.lateration.LocationUtil.Cartesian;
import de.fewclicks.beacon.prototype.lateration.LocationUtil.Cartesian2D;

/**
 * Circular Lateration class
 * <p/>
 * calculates a circular lateration based on distance estimations
 * 
 * @author lukas
 */
public class CircularLateration2D implements ILateration {

	private static final String TAG = "Circular Lateration 2D";
	private double DELTA = 0.01; /* delta value to stop calculations */
	private int MAXITERATIONS = 500;

	private List<GuessCoordinate> locations; /* locations of the objects */
	private Cartesian2D currentEstimation; /* current best estimation */

	/**
	 * calculates the circular lateration for a list of guessed points
	 */
	public CartesianCoordinate getLateration(List<GuessCoordinate> guessPoints) {
		this.locations = guessPoints;

//		double dxsum=0.0;
//		double dysum=0.0;
//		double dzsum=0.0;
//		
//		
//		// get max & min & debug
//		for (int i = 0; i < guessPoints.size(); i++) {
//			dxsum += guessPoints.get(i).getX();
//			dysum += guessPoints.get(i).getY();
//			dzsum += guessPoints.get(i).getZ();
//			
//			Log.d(TAG, "Got: " + guessPoints.get(i).toString());
//		}

		
		
		// will contain the current best estimation
//		this.currentEstimation = new Cartesian(dxsum/guessPoints.size(), dysum/guessPoints.size(), dzsum/guessPoints.size());
		this.currentEstimation = new Cartesian2D(0.0,0.0);
		
		Log.d(TAG, "Start Est:"+this.currentEstimation.toString());
		
		if (guessPoints.size() > 0) {

			// iterate over all points and get arithmetic mean value
			for (GuessCoordinate currentGP : guessPoints) {

				Cartesian2D newDelta = new Cartesian2D(currentGP);
				this.currentEstimation.addDelta(newDelta);
			}
			currentEstimation.setX(Math.abs(currentEstimation.getX()
					/ guessPoints.size()));
			currentEstimation.setY(Math.abs(currentEstimation.getY()
					/ guessPoints.size()));
		}

		calulcateCircularLateration();

		CartesianCoordinate cc= new CartesianCoordinate(currentEstimation);
		
		return cc;
	}

	/**
	 * method which calculates a position approximation using circular
	 * lateration. Will calculate delta/correction vectors which are applied to
	 * current best estimation until the cumulated delta values fall below the
	 * given threshold DELTA
	 */
	private void calulcateCircularLateration() {
		int i = 0;
		double delta = 0.0;
		double lastDelta = 0.0;

		while (i < MAXITERATIONS) {

			Cartesian2D newDelta = calculateDeltaVector();

//			Log.i(TAG, "DeltaVector: " + newDelta.toString());

			// Log.i("GM","Delta: " + newDelta);
			currentEstimation.addDelta(newDelta);

			if (i % 10 == 0) {
				Log.i(TAG, "Iteration " + i);
				Log.i(TAG, currentEstimation.toString() + " - "
						+ currentEstimation);
			}
			delta = newDelta.delta();
			if ((Math.abs((delta - lastDelta)) <= DELTA
					|| (delta > lastDelta && (lastDelta != 0.0)))) {
				Log.d("Lateration", "End on: Delta,lastDelta:" + delta + ","
						+ lastDelta);
				break;
			} else {
				Log.i(TAG, "Delta @" + delta + " Current Est:"
						+ currentEstimation.toString());
				lastDelta = delta;
			}
			i++;
		}
	}

	/**
	 * calculate one step, hence a correction vector for the current best
	 * estimation calculates the matrix A and vector b and solves the equation
	 * system. Returns a correction vector as Cartesian coordinates.
	 */
	private Cartesian2D calculateDeltaVector() {
		Matrix matrixA = calculateMatrixValues();
		Matrix vectorB = calculateVectorValues();

//		Log.d(TAG, "MatrixA: " + matrixA);
//		Log.d(TAG, "vectorB: " + vectorB);

		Cartesian2D deltaVector = new Cartesian2D(0.0, 0.0);

		try {
			// first transpose matrix A for later calculations
			Matrix matrixAT = MatrixMathematics.transpose(matrixA);
			//Log.d(TAG, "matrixAT: " + matrixAT);
			// calculate (A^T * A)^-1 * A^T

//			Log.d(TAG, "AT * A "
//					+ MatrixMathematics.multiply(matrixAT, matrixA).toString());
//
//			Log.d(TAG,
//					"(AT*A)^-1"
//							+ MatrixMathematics.inverse(MatrixMathematics
//									.multiply(matrixAT, matrixA)));

			Matrix matrixCalculations = MatrixMathematics.multiply(
					MatrixMathematics.inverse(MatrixMathematics.multiply(
							matrixAT, matrixA)), matrixAT);
//			Log.d(TAG, "matrixCalculations: " + matrixCalculations);
			// multiply resulting matrix with vector b
			Matrix result = MatrixMathematics.multiply(matrixCalculations,
					vectorB);
//			Log.d(TAG, "result: " + result);
			deltaVector = new Cartesian2D(result.getValueAt(0, 0),
					result.getValueAt(1, 0));
		} catch (NoSquareException e) {
			e.printStackTrace();
		}

		return deltaVector;
	}

	/**
	 * calculates the (haversine) distance between two points on the surface of
	 * a sphere
	 * 
	 * @param baseStation
	 *            - position of the base-station / object
	 * @param terminal
	 *            - own position
	 * @return - distance from haversine formula
	 */
//	private double pseudoRange(Cartesian baseStation, Cartesian terminal) {
//		return LocationUtil.distance(baseStation, terminal);
//
//	}
	
	private double pseudoRange(Cartesian2D baseStation, Cartesian2D terminal) {
		return LocationUtil.distance(baseStation, terminal);

	}

	/**
	 * calculates the values for the matrix A
	 */
	private Matrix calculateMatrixValues() {
		// get a Nx2 matrix
		Matrix matrixA = new Matrix(locations.size(), 2);

		for (int i = 0; i < locations.size(); i++) {
			// some mathematical stuff
			GuessCoordinate currentLocation = locations.get(i);

			Cartesian2D currentLocationCartesian = new Cartesian2D(currentLocation);

			// calculate matrix values a, b, and c for each location
			// see Küpper, LBS, Page 134 / Chapter 6, Circular Lateration
			Double valueA = (currentLocationCartesian.getX() - currentEstimation
					.getX())
					/ pseudoRange(currentLocationCartesian, currentEstimation);

			Double valueB = (currentLocationCartesian.getY() - currentEstimation
					.getY())
					/ pseudoRange(currentLocationCartesian, currentEstimation);

			matrixA.setValueAt(i, 0, valueA);
			matrixA.setValueAt(i, 1, valueB);
		}

		return matrixA;
	}

	/**
	 * calculates the values for the vector b
	 */
	private Matrix calculateVectorValues() {
		// get a vector with N dimensions
		Matrix vectorB = new Matrix(locations.size(), 1);

		// for all location objects
		for (int i = 0; i < locations.size(); i++) {
			GuessCoordinate currentLocation = locations.get(i);
			Cartesian2D currentLocationCartesian = new Cartesian2D(currentLocation);
			// get difference between guessed distance and the pseudo-range for
			// current approximation
			Double vectorValue = (((currentLocation.getGuessDistance()) - pseudoRange(
					currentLocationCartesian, currentEstimation)));

			vectorB.setValueAt(i, 0, vectorValue);
		}

		return vectorB;
	}
}
