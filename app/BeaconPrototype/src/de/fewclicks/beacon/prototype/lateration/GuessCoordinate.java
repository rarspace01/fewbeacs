package de.fewclicks.beacon.prototype.lateration;

import java.text.DecimalFormat;

public class GuessCoordinate extends CartesianCoordinate{

	private double guessDistance = 0.0;
	
	public GuessCoordinate() {
		super();
	}
	
	public GuessCoordinate(double x, double y, double z){
		super(x,y,z);		
	}
	
	public GuessCoordinate(double x, double y, double z, double distance){
		super(x,y,z);
		this.guessDistance = distance;
	}

	public double getGuessDistance() {
		return guessDistance;
	}

	public void setGuessDistance(double guessDistance) {
		this.guessDistance = guessDistance;
	}
	
	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("####.##");
		String returnString = "X: "+df.format(super.x)+" Y: "+df.format(y)+" Z: "+df.format(z)+" Distance: "+df.format(guessDistance)+" m";
		return returnString;
	}
	
}
