package de.fewclicks.beacon.prototype.lateration;

public class LocationUtil {
//	private static double EARTH_RADIUS = 6367450;

	// /**
	// * convert a geolocation (long & lat) into a cartesian coordinate
	// * with 3-dimensional coordinates (X, Y, Z)
	// */
	// public static Cartesian convertLocationToCarthesian(GeoLocation location)
	// {
	// double latRad = Math.toRadians(location.getLatitude());
	// double lonRad = Math.toRadians(location.getLongitude());
	//
	// double x = EARTH_RADIUS * Math.cos(latRad) * Math.cos(lonRad);
	// double y = EARTH_RADIUS * Math.cos(latRad) * Math.sin(lonRad);
	// double z = EARTH_RADIUS * Math.sin(latRad);
	// return new LocationUtil.Cartesian(x, y, z);
	// }

	// /**
	// * convert a 3-dimensional coordinate into a geolocation
	// * (longitude and latitude)
	// */
	// public static GeoLocation convertCartesianToLocation(Cartesian location)
	// {
	// double lat = Math.asin((location.getZ() / EARTH_RADIUS));
	// double lon = Math.atan2(location.getY(), location.getX());
	//
	// GeoLocation converted = new GeoLocation(0.0, 0.0);
	// converted.setLatitude(Math.toDegrees(lat));
	// converted.setLongitude(Math.toDegrees(lon));
	// return converted;
	// }

	// /**
	// * @author denis
	// * @param g1 - First Coordinate
	// * @param g2 - Second Coordinate
	// * @return distance in meters
	// */
	// public static double distance(GeoLocation g1, GeoLocation g2) {
	// double dLat = Math.toRadians(g2.getLatitude()-g1.getLatitude());
	// double dLng = Math.toRadians(g2.getLongitude()-g1.getLongitude());
	// double sindLat = Math.sin(dLat / 2);
	// double sindLng = Math.sin(dLng / 2);
	// double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	// * Math.cos(Math.toRadians(g1.getLatitude())) *
	// Math.cos(Math.toRadians(g2.getLatitude()));
	// double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	// double dist = EARTH_RADIUS * c;
	//
	// return Math.abs(dist);
	// }

	public static double distance(CartesianCoordinate from, CartesianCoordinate to) {
		double distance = 0.0;
		
		double dx=from.x-to.x;
		double dy=from.y-to.y;
		double dz=from.z-to.z;
		
		distance = Math.sqrt(dx*dx+dy*dy+dz*dz);
		
		return distance;
	}
	
	public static double distance(Cartesian2D from, Cartesian2D to) {
		double distance = 0.0;
		
		double dx=from.X-to.X;
		double dy=from.Y-to.Y;
		
		distance = Math.sqrt(dx*dx+dy*dy);
		
		return distance;
	}
	
	public static double distance(Cartesian from, Cartesian to) {
		double distance = 0.0;
		
		double dx=from.X-to.X;
		double dy=from.Y-to.Y;
		double dz=from.Z-to.Z;
		
		distance = Math.sqrt(dx*dx+dy*dy+dz*dz);
		
		return distance;
	}

	/**
	 * Cartesian container for a 3-dimensional coordinate
	 */
	public static class Cartesian {
		private Double X, Y, Z;

		public Cartesian(CartesianCoordinate cc){
			this.X = cc.x;
			this.Y = cc.y;
			this.Z = cc.z;
		}
		
		public Cartesian(Double X, Double Y, Double Z) {
			this.X = X;
			this.Y = Y;
			this.Z = Z;
		}

		public Double delta() {
			return Math.abs(X) + Math.abs(Y) + Math.abs(Z);
		}

		public Double getX() {
			return X;
		}

		public void setX(Double x) {
			X = x;
		}

		public Double getY() {
			return Y;
		}

		public void setY(Double y) {
			Y = y;
		}

		public Double getZ() {
			return Z;
		}

		public void setZ(Double z) {
			Z = z;
		}

		public void addDelta(Cartesian delta) {
			this.X -= delta.getX();
			this.Y -= delta.getY();
		}

		public String toString() {
			StringBuilder s = new StringBuilder();
			s.append("X: ");
			s.append(X);
			s.append(", Y: ");
			s.append(Y);
			s.append(", Z: ");
			s.append(Z);

			return s.toString();
		}
	}
	
	/**
	 * Cartesian container for a 3-dimensional coordinate
	 */
	public static class Cartesian2D {
		private Double X, Y;

		public Cartesian2D(CartesianCoordinate cc){
			this.X = cc.x;
			this.Y = cc.y;
		}
		
		public Cartesian2D(Double X, Double Y) {
			this.X = X;
			this.Y = Y;
		}

		public Double delta() {
			return Math.abs(X) + Math.abs(Y);
		}

		public Double getX() {
			return X;
		}

		public void setX(Double x) {
			X = x;
		}

		public Double getY() {
			return Y;
		}

		public void setY(Double y) {
			Y = y;
		}

		public void addDelta(Cartesian2D delta) {
			this.X -= delta.getX();
			this.Y -= delta.getY();
		}

		public String toString() {
			StringBuilder s = new StringBuilder();
			s.append("X: ");
			s.append(X);
			s.append(", Y: ");
			s.append(Y);

			return s.toString();
		}
	}
}
