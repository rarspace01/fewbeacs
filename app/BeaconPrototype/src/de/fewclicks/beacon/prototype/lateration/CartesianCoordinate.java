package de.fewclicks.beacon.prototype.lateration;

import java.text.DecimalFormat;

import de.fewclicks.beacon.prototype.lateration.LocationUtil.Cartesian;
import de.fewclicks.beacon.prototype.lateration.LocationUtil.Cartesian2D;

public class CartesianCoordinate {

	protected double x = 0.0;
	protected double y = 0.0;
	protected double z = 0.0;

	public CartesianCoordinate() {
	}
	
	public CartesianCoordinate(Cartesian c) {
		this.x = c.getX();
		this.y = c.getY();
		this.z = c.getZ();
	}
	
	public CartesianCoordinate(Cartesian2D c) {
		this.x = c.getX();
		this.y = c.getY();
		this.z = 0;
	}

	public CartesianCoordinate(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("####.##");
		String returnString = "X: "+df.format(x)+" Y: "+df.format(y)+" Z: "+df.format(z);
		return returnString;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public void setCoordinates(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;		
	}
	
}
