package de.fewclicks.beacon.prototype.model.beacon;

import java.util.HashMap;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;
import de.fewclicks.beacon.prototype.io.HttpHelper;
import de.fewclicks.beacon.prototype.lateration.CartesianCoordinate;

public class BeaconDatabase
{

	private static final String TAG = "BeaconDatabase";

	private final HashMap<String, CartesianCoordinate> beaconDb = new HashMap<String, CartesianCoordinate>();

	public String REMOTE_SITE = "http://becks.hardvisions.de/fewbeacons.json";

	public BeaconDatabase()
	{

		String tmpPage = "";

		new RetrieveBeaconsFromJsonTask().execute();

	}

	public HashMap<String, CartesianCoordinate> getBeaconDb()
	{
		return beaconDb;
	}

	public CartesianCoordinate getCoordinateForBeacon(FewBeacon beacon)
	{
		CartesianCoordinate cc = null;

		String searchKey = generateKey(beacon);

		Log.d(TAG, "Getting Value for Key: " + searchKey);

		cc = beaconDb.get(searchKey);

		if (cc != null)
		{
			Log.d(TAG, "Got Value: " + cc.toString());
		}
		else
		{

			for (Entry<String, CartesianCoordinate> entry : beaconDb.entrySet())
			{
				Log.d(TAG, entry.getKey() + "<>" + entry.getValue());
			}
		}

		return cc;
	}

	private String generateKey(FewBeacon beacon)
	{
		String key = "";

		key = beacon.getProximityUuid() + "-" + beacon.getMajor() + "-"
				+ beacon.getMinor();

		return key;
	}

	/**
	 * retrieves the beacons form the given json
	 * 
	 * @param route
	 *            {@link Route} - Route to be drawn
	 */
	public void retrieveBeaconsFromJsonTask()
	{
		new RetrieveBeaconsFromJsonTask().execute();
	}

	/**
	 * Inner class for handling the retrieve Beacons Task task which takes to
	 * long to handle in the main UI thread.
	 * 
	 * @author denis
	 */
	private class RetrieveBeaconsFromJsonTask extends
			AsyncTask<Void, Void, String>
	{

		@Override
		protected String doInBackground(Void... params)
		{
			return HttpHelper.getPage(REMOTE_SITE);
		}

		@Override
		protected void onPostExecute(String sPage)
		{

			// retrieve page

			try
			{
				JSONObject jsonObject = new JSONObject(sPage);

				JSONArray beaconList = jsonObject.getJSONArray("beaconList");

				for (int i = 0; i < beaconList.length(); i++)
				{
					JSONObject jsonCurrentObject = beaconList.getJSONObject(i);

					String uuid = jsonCurrentObject.getString("uuid");
					int major = jsonCurrentObject.getInt("major");
					int minor = jsonCurrentObject.getInt("minor");
					double posx = jsonCurrentObject.getDouble("posx");
					double posy = jsonCurrentObject.getDouble("posy");
					double posz = jsonCurrentObject.getDouble("posz");

					beaconDb.put(uuid + "-" + major + "-" + minor,
							new CartesianCoordinate(posx, posy, posz));
					// retrieve data

				}

			}
			catch (JSONException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
