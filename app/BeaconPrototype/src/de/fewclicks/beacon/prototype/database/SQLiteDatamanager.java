package de.fewclicks.beacon.prototype.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * class for SQLite management
 * 
 * @author denis
 * 
 */
public class SQLiteDatamanager extends SQLiteOpenHelper {

	private static final int DBVERSION = 1;

	public SQLiteDatamanager(Context context) {
		super(context, "fewbeacon.db", null, DBVERSION);
	}

	/**
	 * get called on the creation of the db
	 */
	public void onCreate(SQLiteDatabase db) {
		Log.i("SQLLite", "onCreate SQLite");
		db.execSQL("CREATE TABLE fewbeacons (uuid TEXT NOT NULL, major NUMERIC NOT NULL, minor NUMERIC NOT NULL, posx NUMERIC NOT NULL, posy NUMERIC NOT NULL, posz NUMERIC NOT NULL, PRIMARY KEY(uuid, major, minor));");
		db.execSQL("CREATE TABLE fewlog (userid TEXT NOT NULL, timestamp NUMERIC, posx NUMERIC NOT NULL, posy NUMERIC NOT NULL, posz NUMERIC NOT NULL, PRIMARY KEY(userid, timestamp));");
	}

	/**
	 * get called on the upgrade of the db
	 */
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i("SQLLite", "onUpgrade");
		db.execSQL("DROP TABLE IF EXISTS fewbeacons");
		db.execSQL("DROP TABLE IF EXISTS fewlog");
		this.onCreate(db);
	}

}
