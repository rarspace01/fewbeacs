package de.fewclicks.beacon.prototype.controller.beacon;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;
import de.fewclicks.beacon.prototype.R;
import de.fewclicks.beacon.prototype.activites.MainActivity;
import de.fewclicks.beacon.prototype.database.SQLiteDatamanager;
import de.fewclicks.beacon.prototype.lateration.CartesianCoordinate;
import de.fewclicks.beacon.prototype.lateration.CircularLateration2D;
import de.fewclicks.beacon.prototype.lateration.GuessCoordinate;
import de.fewclicks.beacon.prototype.model.beacon.BeaconDatabase;
import de.fewclicks.beacon.prototype.model.beacon.FewBeacon;

public class ScanService extends Service {

	public static int SCAN_PERIOD = 10000;

	private Handler mHandler;

	private BluetoothAdapter mBluetoothAdapter;

	protected boolean mScanning;

	public static final int MSG_SET_SCAN_PERIODS = 6;

	public static final int MSG_START_MONITORING = 4;

	public static final int MSG_START_RANGING = 2;

	public static final int MSG_STOP_MONITORING = 5;

	public static final int MSG_STOP_RANGING = 3;

	public static final String TAG = "ScanService";

	private Date lastIBeaconDetectionTime = new Date();

	private final List<FewBeacon> simulatedScanData = null;

	private final HashMap<String, FewBeacon> trackedBeacons = new HashMap<String, FewBeacon>();

	private final IBinder mBinder = new ScanServiceBinder();

	private BeaconDatabase beaconDb;

	public class ScanServiceBinder extends Binder {
		public ScanService getService() {
			return ScanService.this;
		}

	}

	@Override
	public IBinder onBind(Intent intent) {

		// code

		Log.i(TAG, "service created");

		mHandler = new Handler();

		// set Coordinates from DB
		beaconDb = new BeaconDatabase();

		// Use this check to determine whether BLE is supported on the device.
		// Then you can
		// selectively disable BLE-related features.
		if (!getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_BLUETOOTH_LE)) {
			Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT)
					.show();
		}

		// Initializes a Bluetooth adapter. For API level 18 and above, get a
		// reference to
		// BluetoothAdapter through BluetoothManager.
		final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = bluetoothManager.getAdapter();

		// Checks if Bluetooth is supported on the device.
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, R.string.error_bluetooth_not_supported,
					Toast.LENGTH_SHORT).show();
		}

		scanLeDevice(true);

		return mBinder;
	}

	private void scanLeDevice(final boolean enable) {
		if (enable) {
			// Stops scanning after a pre-defined scan period.
			mHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					mScanning = false;
					mBluetoothAdapter.stopLeScan(mLeScanCallback);
				}
			}, SCAN_PERIOD);

			mScanning = true;
			mBluetoothAdapter.startLeScan(mLeScanCallback);
		} else {
			mScanning = false;
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
		}
	}

	// Device scan callback.
	private final BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi,
				final byte[] scanRecord) {
			Runnable scanRunnable = new Runnable() {
				@Override
				public void run() {

					ScanService.ScanProcessor localScanProcessor = new ScanService.ScanProcessor();
					ScanService.ScanData[] arrayOfScanData = new ScanService.ScanData[1];
					arrayOfScanData[0] = new ScanService.ScanData(device, rssi,
							scanRecord);
					localScanProcessor.execute(arrayOfScanData);

				}
			};

			new Thread(scanRunnable).start();

		}
	};

	@Override
	public void onDestroy() {
		// code
		super.onDestroy();
	}

	private class ScanProcessor extends
			AsyncTask<ScanService.ScanData, Void, Void> {

		@Override
		protected Void doInBackground(ScanService.ScanData... paramVarArgs) {
			ScanService.ScanData localScanData = paramVarArgs[0];
			FewBeacon localIBeacon = FewBeacon.fromScanData(
					localScanData.scanRecord, localScanData.rssi);
			if (localIBeacon != null) {
				ScanService.this.processIBeaconFromScan(localIBeacon);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void paramVoid) {

			// mLeDeviceListAdapter.notifyDataSetChanged();

		}

		@Override
		protected void onPreExecute() {
		}

		@Override
		protected void onProgressUpdate(Void... paramVarArgs) {
		}
	}

	private void processIBeaconFromScan(FewBeacon paramIBeacon) {
		this.lastIBeaconDetectionTime = new Date();
		if (this.trackedBeacons.containsKey(paramIBeacon.hash())) {

			FewBeacon localBeacon = this.trackedBeacons
					.get(paramIBeacon.hash());

			if (localBeacon.getRunningAverageRssi() == null) {
				localBeacon.setRunningAverageRssi((double) localBeacon
						.getRssi());
			}

			localBeacon.setRunningAverageRssi(localBeacon
					.getRunningAverageRssi()
					* 0.7
					+ paramIBeacon.getRssi()
					* 0.3);

			Log.d(TAG, "Updated Beacon");
		} else {
			Log.d(TAG, "Added Beacon");
			trackedBeacons.put(paramIBeacon.hash(), paramIBeacon);
		}
		Log.d(TAG, "iBeacon detected :" + paramIBeacon.getProximityUuid() + " "
				+ paramIBeacon.getMajor() + " " + paramIBeacon.getMinor()
				+ " accuracy: " + paramIBeacon.getAccuracy() + " proximity: "
				+ paramIBeacon.getProximity());
		// Iterator localIterator1 = matchingRegions(paramIBeacon,
		// this.monitoredRegionState.keySet()).iterator();
		// while (localIterator1.hasNext()) {
		// Region localRegion2 = (Region) localIterator1.next();
		// MonitorState localMonitorState = (MonitorState)
		// this.monitoredRegionState
		// .get(localRegion2);
		// if (localMonitorState.markInside()) {
		// localMonitorState.getCallback().call(
		// this,
		// "monitoringData",
		// new MonitoringData(localMonitorState.isInside(),
		// localRegion2));
		// }
		// }
		// Log.d(TAG, "looking for ranging region matches for this ibeacon");
		// Iterator localIterator2 = matchingRegions(paramIBeacon,
		// this.rangedRegionState.keySet()).iterator();
		// while (localIterator2.hasNext()) {
		// Region localRegion1 = (Region) localIterator2.next();
		// Log.d(TAG, "matches ranging region: " + localRegion1);
		// ((RangeState) this.rangedRegionState.get(localRegion1))
		// .addIBeacon(paramIBeacon);
		// }
	}

	private class ScanData {
		public BluetoothDevice device;

		public int rssi;

		public byte[] scanRecord;

		public ScanData(BluetoothDevice paramBluetoothDevice, int paramInt,
				byte[] paramArrayOfByte) {
			this.device = paramBluetoothDevice;
			this.rssi = paramInt;
			this.scanRecord = paramArrayOfByte;
		}
	}

	public CartesianCoordinate getPosition() {
		DecimalFormat df = new DecimalFormat("####.#");
		Log.i(TAG, "Device Count:" + trackedBeacons.size());

		List<GuessCoordinate> guessList = new LinkedList<GuessCoordinate>();
		List<FewBeacon> beaconList = new LinkedList<FewBeacon>();

		Iterator beaconIterator = trackedBeacons.entrySet().iterator();
		while (beaconIterator.hasNext()) {
			Entry entry = (Entry) beaconIterator.next();

			FewBeacon localBeacon = (FewBeacon) entry.getValue();

			beaconList.add(localBeacon);
		}

		CartesianCoordinate ccl;

		for (int i = 0; i < beaconList.size(); i++) {
			ccl = beaconDb.getCoordinateForBeacon(beaconList.get(i));
			Log.i(TAG,
					"" + beaconList.get(i).getProximityUuid() + " "
							+ beaconList.get(i).getMajor() + "-"
							+ beaconList.get(i).getMinor() + "  "
							+ df.format(beaconList.get(i).getAccuracy())
							+ "m - " + ccl.toString());
			guessList.add(new GuessCoordinate(ccl.getX(), ccl.getY(), ccl
					.getZ(), beaconList.get(i).getAccuracy() * 1));
		}

		// calc 2D
		CircularLateration2D cl2 = new CircularLateration2D();
		CartesianCoordinate cc2 = cl2.getLateration(guessList);
		Log.d(TAG, "2D: " + cc2.toString());

		// // calc 3D
		// CircularLateration cl = new CircularLateration();
		// CartesianCoordinate cc3 = cl.getLateration(guessList);
		// Log.d(TAG, "3D " + cc3.toString());

		Context context = getApplicationContext();
		CharSequence text = "2D: " + cc2.toString();
		int duration = Toast.LENGTH_LONG;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();

		// NotificationProcessor np = new NotificationProcessor();
		// np.execute();

		// save Locatoin to DB
		logLocation(cc2);

		return cc2;
	}

	private void logLocation(CartesianCoordinate cc2) {
		SQLiteDatamanager dbManager = new SQLiteDatamanager(this);
		SQLiteDatabase dbConn_ = dbManager.getWritableDatabase();

		// sql statement preparement
		ContentValues contentValues = new ContentValues();
		contentValues.put("userid", UUID.randomUUID().toString());
		contentValues.put("timestamp", System.currentTimeMillis());
		contentValues.put("posx", cc2.getX());
		contentValues.put("posy", cc2.getY());
		contentValues.put("posz", cc2.getZ());

		// insert qry
		dbConn_.insert("fewlog", null, contentValues);

		// close connection and manager properly
		dbConn_.close();
		dbManager.close();

	}

	public boolean pushDataToServer(){
		boolean isSuccessfull = false;
		
		//get from db
		
		return isSuccessfull;
	}
	
	private class NotificationProcessor extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			printNotification();
			return null;
		}

	}

	private void printNotification() {

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(getResources().getString(R.string.app_name))
				.setContentText("Notification!");
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(this, MainActivity.class);

		// The stack builder object will contain an artificial back stack for
		// the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(MainActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);

		mBuilder.setLights(Color.RED, 500, 500);

		long pattern[] = { 500, 500, 500 };

		mBuilder.setVibrate(pattern);

		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify((int) (Math.random() * 1000),
				mBuilder.build());

	}

	public BeaconDatabase getBeaconDb() {
		return beaconDb;
	}

}
