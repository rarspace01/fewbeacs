package de.fewclicks.beacon.prototype.controller.beacon;

public interface IServiceObserver {

	public void notify(FewBeaconNotification notification);
	
}
