package de.fewclicks.beacon.prototype.controller.map;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import de.fewclicks.beacon.prototype.lateration.CartesianCoordinate;

public class MapView extends View
{

	private static final String Tag = "MapView";

	private ShapeDrawable map;

	private final Path path = new Path();

	private final Paint paint = new Paint();

	private int maxWidth;

	private int maxHheight;

	private int minWidth;

	private int minHheight;

	private final double maxXCoordinate = 100d;

	private final double maxYCoordinate = 50d;

	private final double minXCoordinate = 0d;

	private final double minYCoordinate = 0d;

	private double coordinatePixelSize = 0;

	private Point position = new Point(-1, -1);

	private final List<Point> points = new LinkedList<Point>();

	private final float radius = 20;

	public MapView(Context context)
	{

		super(context);

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		minWidth = 0;
		minHheight = 0;
		maxWidth = this.getMeasuredWidth();
		maxHheight = this.getMeasuredHeight();
		initCoordinatePixelSize();
		initPaint();
		if (map == null)
		{
			map = new ShapeDrawable(new PathShape(path, maxWidth, maxHheight));
		}
		map.getPaint().set(paint);
		map.setBounds(minWidth, minHheight, maxWidth, maxHheight);
		Log.i(Tag, "Initialized Map View: coordinatePixelSize="
				+ coordinatePixelSize + ", maxWidth=" + maxWidth
				+ ", maxHheight=" + maxHheight + ", maxXCoordinate="
				+ maxXCoordinate + ", maxYCoordinate=" + maxYCoordinate);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	private void setBorder()
	{
		path.moveTo(0, 0);
		path.lineTo(0, (float) (coordinatePixelSize * maxYCoordinate));
		path.lineTo((float) (coordinatePixelSize * maxXCoordinate),
				(float) (coordinatePixelSize * maxYCoordinate));
		path.lineTo((float) (coordinatePixelSize * maxXCoordinate), 0);
		path.lineTo(0, 0);

	}

	private void initPaint()
	{
		paint.setColor(Color.BLACK);
		paint.setStrokeWidth(3);
		paint.setStyle(Paint.Style.STROKE);
	}

	private void initCoordinatePixelSize()
	{
		coordinatePixelSize = 0;
		Log.i(Tag, "maxWidth=" + maxWidth + ", maxHheight=" + maxHheight);
		if ((maxWidth / maxXCoordinate) < (maxHheight / maxYCoordinate))
		{
			coordinatePixelSize = maxWidth / maxXCoordinate;
		}
		else
		{
			coordinatePixelSize = maxHheight / maxYCoordinate;
		}
		Log.i(Tag, "coordinatePixelSize=" + coordinatePixelSize);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (event.getAction() != MotionEvent.ACTION_UP)
		{
			Point point = new Point();
			point.x = (int) event.getX();
			point.y = (int) event.getY();
			addPoint(point);
			return true;
		}
		return super.onTouchEvent(event);
	}

	public void setPosition(CartesianCoordinate cartesianCoordinate)
	{
		int coordinatePixelXPosition = (int) (cartesianCoordinate.getX() * coordinatePixelSize);
		int coordinatePixelYPosition = (int) (cartesianCoordinate.getY() * coordinatePixelSize);
		position = new Point(coordinatePixelXPosition, coordinatePixelYPosition);
		Log.i(Tag, "Set Position to: x=" + coordinatePixelXPosition + "px, y="
				+ coordinatePixelYPosition + "px");
		this.invalidate();

	}

	public void addPoints(List<Point> points)
	{
		points.addAll(points);
	}

	public void addPoint(Point point)
	{
		points.add(point);
		invalidate();
	}

	@Override
	public void onDraw(Canvas canvas)
	{
		// addPointsToPath();
		setBorder();
		canvas.drawPath(path, paint);
		if (position != null && position.x >= 0 && position.y >= 0)
		{
			path.addCircle(position.x, position.y, radius, Path.Direction.CCW);
		}
		canvas.drawPath(path, paint);
	}

	private void addPointsToPath()
	{
		boolean first = true;
		for (Point point : points)
		{
			if (first)
			{
				first = false;
				path.moveTo(point.x, point.y);
			}
			else
			{
				path.lineTo(point.x, point.y);
			}
		}
	}

}
