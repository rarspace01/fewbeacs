package de.fewclicks.beacon.prototype.controller.beacon;

public interface IServiceObservable {
	
	/**
	 * register on Subject
	 * @param obj
	 */
	public void registerObserver(Object obj);
	
	/**
	 * unregister on Subject
	 * @param obj
	 */
	public void unregisterObserver(Object obj);
	
	/**
	 * notifies the Observers
	 */
	public void notifyObservers();
	
}
